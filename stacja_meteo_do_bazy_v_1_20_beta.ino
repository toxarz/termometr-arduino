#include <ESP8266WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <HttpClient.h>
#include "DHT.h"
//connection settings 
const char* ssid= "Politbiuro";
const char* password = "88888888";
const int httpPort = 80;
const char* host = "www.serwer2043067.home.pl"; //IP serwera z MySQL


// Outside sensor cofnig
const int oneWireBus = 14;  
 // Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);
// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensors(&oneWire);


//Light sensor
int lightvalue = 0;
const int lightPin = 5;  // ESP8266 Analog Pin ADC0 = A0


//Indoor temperature and humidity sensor
#define DHTPIN 12  
#define DHTTYPE DHT22 
float Temperature;
float Humidity;
DHT dht(DHTPIN, DHTTYPE);


// to check always but send every few times 
const long basetime = 900000 ; //in ms before next update to database
unsigned long previousTime_1 = 0;


void setup() {
 Serial.begin(115200);


  WiFi.begin(ssid, password);  
 while (WiFi.status() != WL_CONNECTED) {
   delay(500);
   Serial.print(".");
 }
Serial.println(WiFi.localIP());

 
    dht.begin();

}





void loop() {


  //outside sensor
   sensors.requestTemperatures(); 
  float temperatureC = sensors.getTempCByIndex(0);


  //light sensor
  lightvalue =  digitalRead(lightPin);
   if (lightvalue == 1) {
    lightvalue=0;
  } else {
    lightvalue=1;
  }
   
 // inside sensor
 float h = dht.readHumidity();
  float t = dht.readTemperature();
  


   //show values
     Serial.print("Temperature OUTSIDE = ");
     Serial.print(temperatureC);
     Serial.println("ºC");
     
  Serial.print("Light= ");
  Serial.println(lightvalue);
   
   Serial.print("Temp inside = ");
  Serial.print(t);
  Serial.print(" HUM inside = ");
  Serial.println(h);

  //send values to database

  WiFiClient client; //wysylanie do PHP i MySQL
  if (!client.connect(host, httpPort)) {
   Serial.println("connection failed");
   
 }
 else
 {


 /* Updates frequently */
 unsigned long currentTime = millis();
/* This is my event_1 */
  if ( currentTime - previousTime_1 >= basetime) {
    String url = "http://serwer2043067.home.pl/arduino/write_data.php?value=";
   url += temperatureC;
   url +="&temphome=";
   url += t;
   url += "&humidity=";
   url += h;
   url += "&light=";
   url += lightvalue; 
   client.print(String("GET ") + url + " HTTP/1.1\r\n" + "Host: " + host + "\r\n" + "Connection: close\r\n\r\n");  
   Serial.println(String("GET ") + url + " HTTP/1.1\r\n" + "Host: " + host + "\r\n" + "Connection: close\r\n\r\n"); 
   
    /* Update the timing for the next event*/
    previousTime_1 = currentTime;
  }
  

   
} 
}
